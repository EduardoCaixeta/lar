package view.forms.login;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;

import controller.UsersController;
import model.Database;
import model.entity.Users;
import view.Frame;
import view.panels.login.PanelSingUp;

public class FormSingUp {

	private PanelSingUp panel;
	private UsersController controllerUser;
	private Frame frame;

	@SuppressWarnings("deprecation")
	public FormSingUp(PanelSingUp panel, Frame frame) throws Exception {
		this.panel = panel;
		controllerUser = new UsersController(new Database(frame.getNameDBU()));
		this.frame = frame;

		panel.getTxtUsuario().addActionListener(e -> {
			if (panel.getTxtUsuario().getText().isEmpty())
				panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			else {
				panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
				panel.getTxtSenha().requestFocus();
			}
		});
		panel.getTxtSenha().addActionListener(e -> {
			if (panel.getTxtSenha().getText().isEmpty())
				panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			else {
				panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
				panel.getTxtConfSenha().requestFocus();
			}
		});
		panel.getTxtConfSenha().addActionListener(e -> {
			if (panel.getTxtConfSenha().getText().isEmpty())
				panel.getTxtConfSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			else {
				panel.getTxtConfSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
				panel.getTxtEmail().requestFocus();
			}
		});
		panel.getTxtEmail().addActionListener(e -> singUp_Click());

		panel.getButtonSingUp().addActionListener(e -> singUp_Click());
		panel.getButtonSingUp().setMnemonic(KeyEvent.VK_S);
		panel.getButtonSingUp().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonSingUp().doClick();
				}
			}
		});

		panel.getButtonVoltar().addActionListener(e -> voltar_Click());
		panel.getButtonVoltar().setMnemonic(KeyEvent.VK_S);
		panel.getButtonVoltar().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					panel.getButtonVoltar().doClick();
				}
			}
		});

	}

	public void voltar_Click() {
		frame.getContentPane().removeAll();
		frame.setLogin();
	}

	public void singUp_Click() {
		try {
			Users user = getDadosSingUp();
			controllerUser.create(user);
			JOptionPane.showMessageDialog(frame, "Cadastro realizado com sucesso.", "CADASTRO REALIZADO.",
					JOptionPane.INFORMATION_MESSAGE);
			frame.getContentPane().removeAll();
			frame.setLogin();

		} catch (Exception e) {
			if (e.getMessage().toUpperCase().equals("~") == false)
				JOptionPane.showMessageDialog(frame, e.getMessage().toUpperCase(), "Algo deu errado.",
						JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public boolean isValiidadEmail(String email) {

		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

	@SuppressWarnings("deprecation")
	public Users getDadosSingUp() throws Exception {
		Users user = new Users();
		panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getTxtConfSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getTxtEmail().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		if (panel.getTxtUsuario().getText().trim().isEmpty() == false) {
			if(panel.getTxtUsuario().getText().length()>=4 && panel.getTxtUsuario().getText().length()<=15)
			{
			if (controllerUser.existUsername(panel.getTxtUsuario().getText()) == false) {
				user.setUsername(panel.getTxtUsuario().getText());
				if (panel.getTxtSenha().getText().isEmpty() == false) {
					if (panel.getTxtSenha().getText().length() >= 5 && panel.getTxtSenha().getText().length() <= 12) {
						if (panel.getTxtConfSenha().getText().isEmpty() == false) {
							if (panel.getTxtConfSenha().getText().equals(panel.getTxtSenha().getText())) {
								user.setPassword(panel.getTxtSenha().getText());
								if (panel.getTxtEmail().getText().trim().isEmpty() == false) {
									if (isValiidadEmail(panel.getTxtEmail().getText().trim())) {
										if (controllerUser.existEmail(panel.getTxtEmail().getText().trim()) == false) {
											user.setEmail(panel.getTxtEmail().getText());
											return user;
										} else {
											panel.getTxtEmail()
													.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
											panel.getTxtEmail().requestFocus();
											panel.getTxtEmail().setText("");
											throw new Exception("Endere�o de email j� cadastrado.");
										}
									} else {
										panel.getTxtEmail()
												.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
										panel.getTxtEmail().requestFocus();
										throw new Exception("Endere�o de email inv�lido.");
									}
								} else {
									panel.getTxtEmail()
											.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
									panel.getTxtEmail().requestFocus();
									throw new Exception("~");
								}
							} else {
								panel.getTxtConfSenha()
										.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
								panel.getTxtConfSenha().requestFocus();
								panel.getTxtConfSenha().setText("");
								throw new Exception("As senhas n�o conferem.");
							}
						} else {
							panel.getTxtConfSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
							panel.getTxtConfSenha().requestFocus();
							throw new Exception("~");
						}
					} else {
						panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
						panel.getTxtSenha().requestFocus();
						panel.getTxtSenha().setText("");
						panel.getTxtConfSenha().setText("");
						throw new Exception("A senha deve conter de 5 a 12 caracteres.");
					}
				} else {
					panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
					panel.getTxtSenha().requestFocus();
					throw new Exception("~");
				}
			} else {
				panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				panel.getTxtUsuario().requestFocus();
				throw new Exception("Nome de usu�rio n� cadastrado.");
			}
			}
			else
			{
				panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				throw new Exception("O nome de usu�rio deve conter entre 4 e 15 caracteres.");
			}
		} else {
			panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			panel.getTxtUsuario().requestFocus();
			throw new Exception("~");
		}
	}

	public PanelSingUp getPanel() {
		return panel;
	}

	public void setPanel(PanelSingUp panel) {
		this.panel = panel;
	}

	public UsersController getControllerUser() {
		return controllerUser;
	}

	public void setControllerUser(UsersController controllerUser) {
		this.controllerUser = controllerUser;
	}

	public Frame getFrame() {
		return frame;
	}

	public void setFrame(Frame frame) {
		this.frame = frame;
	}
}
