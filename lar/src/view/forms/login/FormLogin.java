package view.forms.login;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.UsersController;
import model.Database;
import model.entity.Users;
import view.Frame;
import view.panels.login.PanelHelpPass;
import view.panels.login.PanelLogin;
import view.panels.login.PanelSingUp;

public class FormLogin {

	private PanelLogin panel;
	private UsersController controllerUser;
	private Frame frame;
	public FormLogin(PanelLogin panel,Frame frame) throws Exception
	{
		this.panel = panel;
		controllerUser = new UsersController(new Database(frame.getNameDBU()));
		this.frame = frame;
		
		panel.getTxtUsuario().addActionListener(e -> {
			if(panel.getTxtUsuario().getText().isEmpty()) panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			else
			{
				panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
				panel.getTxtSenha().requestFocus();
			}
			});
		panel.getTxtSenha().addActionListener(e -> login_Click());
		
		panel.getButtonLogin().addActionListener(e -> login_Click());
		panel.getButtonLogin().setMnemonic(KeyEvent.VK_S);
        panel.getButtonLogin().addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        panel.getButtonLogin().doClick();
                    }
                }
            });
        
        panel.getButtonSingUp().addActionListener(e -> singUp_Click());
		panel.getButtonSingUp().setMnemonic(KeyEvent.VK_S);
        panel.getButtonSingUp().addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        panel.getButtonSingUp().doClick();
                    }
                }
            });
        panel.getButtonHelpPass().addActionListener(e -> helpPass_Click());
		panel.getButtonHelpPass().setMnemonic(KeyEvent.VK_S);
        panel.getButtonHelpPass().addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        panel.getButtonHelpPass().doClick();
                    }
                }
            });
        
	}
	
	public void helpPass_Click()
	{
		try
		{
			int width = frame.getWidth();
			int height = frame.getHeight();
			frame.getContentPane().removeAll();
			FormHelpPass formSingUp = new FormHelpPass(new PanelHelpPass(), frame);
			frame.add(formSingUp.getPanel());
			frame.pack();
			frame.setSize(width, height);
			formSingUp.getPanel().getTxtEmail().requestFocus();
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(frame,e.getMessage().toUpperCase(),"Algo deu errado", 
	                JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	public void clearTextFields()
    {
        Component components[] = panel.getComponents();

        for (Component c: components)
        {
            if (c instanceof JTextField)
                ((JTextField) c).setText("");
        }   
    }
	
	public void login_Click()
	{
		try
		{
			Users user = getDadosLogin();
			if(controllerUser.existUsers(user) == false)
			{
				clearTextFields();
				panel.getTxtUsuario().requestFocus();
				throw new Exception("Usu�rio e/ou senha incorreto(s).\nTente novamente.");
				
			}
			else
			{
				frame.setResizable(true);
				frame.dispose();
				frame.setSize(720,534);
				frame.centerForm();
				frame.setHome(controllerUser.getUser(user.getUsername(), "username"));
				frame.pack();
				frame.setVisible(true);
				frame.setSize(720,534);
			}
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(frame,e.getMessage().toUpperCase(),"Algo deu errado", 
	                JOptionPane.INFORMATION_MESSAGE);
		}
		
	}
	
	public void singUp_Click()
	{
		try
		{
			int width = frame.getWidth();
			int height = frame.getHeight();
			frame.getContentPane().removeAll();
			FormSingUp formSingUp = new FormSingUp(new PanelSingUp(), frame);
			frame.add(formSingUp.getPanel());
			frame.pack();
			frame.setSize(width, height);
			formSingUp.getPanel().getTxtUsuario().requestFocus();
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(frame,e.getMessage().toUpperCase(),"Algo deu errado", 
	                JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	
	@SuppressWarnings("deprecation")
	public Users getDadosLogin() throws Exception
    {
        Users username = new Users();
        panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        if(panel.getTxtUsuario().getText().trim().isEmpty() == false )
        {
            username.setUsername(panel.getTxtUsuario().getText());
            if(panel.getTxtSenha().getText().isEmpty() == false)
            {
                username.setPassword(panel.getTxtSenha().getText());
                return username;
            }
            else 
            {
                panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                panel.getTxtSenha().requestFocus();
                throw new Exception("Insira a senha.");
            }
        }
        else
        {
            panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
            panel.getTxtUsuario().requestFocus();
            throw new Exception("Insira o usu�rio.");
        }
    }

	public PanelLogin getPanel() {
		return panel;
	}

	public void setPanel(PanelLogin panel) {
		this.panel = panel;
	}

	public UsersController getControllerUser() {
		return controllerUser;
	}

	public void setControllerUser(UsersController controllerUser) {
		this.controllerUser = controllerUser;
	}
	
}
