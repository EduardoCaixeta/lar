package view.forms.configs;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import controller.ProdutosController;
import controller.UsersController;
import controller.ValidadeController;
import model.Database;
import view.Frame;
import view.panels.PanelConfig;

public class FormConfig {
	private PanelConfig panel;
    private UsersController controllerUser;
    private ProdutosController controllerProduto;
    private ValidadeController controllerValidade;
    private Frame frame;
    public FormConfig(PanelConfig panel, Frame frame) {
    	this.panel = panel;
    	this.frame = frame;
    	try
    	{
    		controllerUser = new UsersController(new Database(frame.getNameDBU()));
    		controllerProduto = new ProdutosController(new Database(frame.getNameDBP()));
    		controllerValidade = new ValidadeController(new Database(frame.getNameDBV()));
    	}
    	catch(Exception e)
    	{

    	}
    	panel.getButtonAltUsername().addActionListener(e -> altUsername_Click());
    	panel.getButtonAltUsername().setMnemonic(KeyEvent.VK_S);
        panel.getButtonAltUsername().addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        panel.getButtonAltUsername().doClick();
                    }
                }
            });
        panel.getButtonAltPass().addActionListener(e -> altPassword_Click());
    	panel.getButtonAltPass().setMnemonic(KeyEvent.VK_S);
        panel.getButtonAltPass().addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        panel.getButtonAltPass().doClick();
                    }
                }
            });
        
        panel.getButtonAltEmail().addActionListener(e -> altEmail_Click());
    	panel.getButtonAltEmail().setMnemonic(KeyEvent.VK_S);
        panel.getButtonAltEmail().addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        panel.getButtonAltEmail().doClick();
                    }
                }
            });
        
 
        seeQnts();
    }
    
    public void seeQnts()
    {
    	try
        {
        	panel.getTxtQntProd().setText(String.valueOf(controllerProduto.readAll().size()));
        	panel.getTxtQntVali().setText(String.valueOf(controllerValidade.readAll().size()));
        }catch(Exception e)
        {
        	JOptionPane.showMessageDialog(frame,e.getMessage().toUpperCase(),"Algo deu errado", 
	                JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
   
    public void altUsername_Click() {
    	try
    	{
    		new ConfigAltUsername(frame);
    	}
    	catch(Exception e)
    	{
    		JOptionPane.showMessageDialog(frame,e.getMessage().toUpperCase(),"Algo deu errado", 
	                JOptionPane.INFORMATION_MESSAGE);
    	}
    }
    
    public void altPassword_Click() {
    	try
    	{
    		new ConfigAltPassword(frame);
    	}
    	catch(Exception e)
    	{	
    		JOptionPane.showMessageDialog(frame,e.getMessage().toUpperCase(),"Algo deu errado", 
	                JOptionPane.INFORMATION_MESSAGE);
    	}
    }

    public void altEmail_Click()
    {
    	try
    	{
    		new ConfigAltEmail(frame);
    	}
    	catch(Exception e)
    	{	
    		JOptionPane.showMessageDialog(frame,e.getMessage().toUpperCase(),"Algo deu errado", 
	                JOptionPane.INFORMATION_MESSAGE);
    	}
    }
    
    
    
    
	public PanelConfig getPanel() {
		return panel;
	}

	public void setPanel(PanelConfig panel) {
		this.panel = panel;
	}

	public UsersController getControllerUser() {
		return controllerUser;
	}

	public void setControllerUser(UsersController controllerUser) {
		this.controllerUser = controllerUser;
	}

	public Frame getFrame() {
		return frame;
	}

	public void setFrame(Frame frame) {
		this.frame = frame;
	}

}
