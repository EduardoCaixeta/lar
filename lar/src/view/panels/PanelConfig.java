package view.panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import model.Database;
import view.Frame;

@SuppressWarnings("serial")
public class PanelConfig extends javax.swing.JPanel {

    /**
     * Creates new form PanelConfig
     */
    public PanelConfig(Frame frame) {
        initComponents(frame);
        manipularComponents();
    }
    
    public void paintComponent(Graphics g) {
		g.setColor(new Color(190, 190, 190));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

	} 
   
    
	private void initComponents(Frame frame) {

        labelTitulo = new javax.swing.JLabel();
        scrollPane = new javax.swing.JScrollPane();
        panel = new javax.swing.JPanel() {
        	@Override
        	public void paintComponent(Graphics g) {
        		g.setColor(new Color(190, 190, 190));
        		g.fillRect(0, 0, this.getWidth(), this.getHeight());

        	}
        };
        separador = new javax.swing.JSeparator();
        labelInfConta = new javax.swing.JLabel();
        separador1 = new javax.swing.JSeparator();
        labelBacukp = new javax.swing.JLabel();
        labelQntProd = new javax.swing.JLabel();
        labelQntVali = new javax.swing.JLabel();
        txtQntProd = new javax.swing.JTextField();
        txtQntVali = new javax.swing.JTextField();
        buttonMoreProd = new javax.swing.JButton();
        buttonMoreVali = new javax.swing.JButton();
        separdor2 = new javax.swing.JSeparator();
        labelAdicionais = new javax.swing.JLabel();
        checkExbProd = new javax.swing.JCheckBox();
        checkStartLogin = new javax.swing.JCheckBox();
        buttonAltUsername = new javax.swing.JButton();
        buttonAltEmail = new javax.swing.JButton();
        buttonAltPass = new javax.swing.JButton();
        buttonBackup = new javax.swing.JButton();
        buttonTxtVali = new javax.swing.JButton();
        buttonTxtProduto = new javax.swing.JButton();
        buttonDesenvolver = new javax.swing.JButton();

        labelTitulo.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        labelTitulo.setText("CONFIGURA��ES");

        labelInfConta.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelInfConta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelInfConta.setText("INFORMA��ES DA CONTA");
        labelInfConta.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        labelBacukp.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelBacukp.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelBacukp.setText("BACUKP");
        labelBacukp.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        labelQntProd.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelQntProd.setText("Produtos cadastrados:");

        labelQntVali.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelQntVali.setText("Validade cadastradas:");

        txtQntProd.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtQntProd.setEditable(false);
        txtQntProd.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        
        txtQntVali.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtQntVali.setEditable(false);
        txtQntVali.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        buttonMoreProd.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonMoreProd.setText("+");


        buttonMoreVali.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonMoreVali.setText("+");

        labelAdicionais.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelAdicionais.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelAdicionais.setText("ADICIONAIS");
        labelAdicionais.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        checkExbProd.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        checkExbProd.setText("Exibir se h� produtos vencidos");
        checkExbProd.setOpaque(false);
        checkExbProd.setFocusPainted(false);
        checkExbProd.setFocusable(false);
        checkExbProd.setHideActionText(true);
        
        checkStartLogin.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        checkStartLogin.setText("Iniciar com tela de Login");
        checkStartLogin.setOpaque(false);
        checkStartLogin.setFocusPainted(false);
        checkStartLogin.setFocusable(false);
        checkStartLogin.setHideActionText(true);

        buttonAltUsername.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonAltUsername.setText("Alterar Usu�rio");
        buttonAltUsername.setFocusPainted(false);
        buttonAltUsername.setFocusable(false);
        buttonAltUsername.setHideActionText(true);
        buttonAltUsername.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);


        buttonAltEmail.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonAltEmail.setText("Alterar Email");
        buttonAltEmail.setFocusPainted(false);
        buttonAltEmail.setFocusable(false);
        buttonAltEmail.setHideActionText(true);
        buttonAltEmail.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        buttonAltPass.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonAltPass.setText("Alterar Senha");
        buttonAltPass.setFocusPainted(false);
        buttonAltPass.setFocusable(false);
        buttonAltPass.setHideActionText(true);
        buttonAltPass.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        buttonBackup.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonBackup.setText("Salvar Dados");
        buttonBackup.setFocusPainted(false);
        buttonBackup.setFocusable(false);
        buttonBackup.setHideActionText(true);
        buttonBackup.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        buttonTxtVali.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonTxtVali.setText("Gerar TXT Validades");
        buttonTxtVali.setFocusPainted(false);
        buttonTxtVali.setFocusable(false);
        buttonTxtVali.setHideActionText(true);
        buttonTxtVali.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        buttonTxtProduto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonTxtProduto.setText("Gerar TXT Produtos");
        buttonTxtProduto.setFocusPainted(false);
        buttonTxtProduto.setFocusable(false);
        buttonTxtProduto.setHideActionText(true);
        buttonTxtProduto.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        buttonDesenvolver.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonDesenvolver.setText("Atalhos do desenvolvedor");
        buttonDesenvolver.setFocusPainted(false);
        buttonDesenvolver.setFocusable(false);
        buttonDesenvolver.setHideActionText(true);
        buttonDesenvolver.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        
        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
                panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(separador, javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(separador1, javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(separdor2)
                .addGroup(panelLayout.createSequentialGroup()
                    .addContainerGap(281, Short.MAX_VALUE)
                    .addComponent(labelBacukp, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 279, Short.MAX_VALUE))
                .addGroup(panelLayout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(panelLayout.createSequentialGroup()
                            .addGap(73, 73, 73)
                            .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(buttonTxtVali)
                                .addGroup(panelLayout.createSequentialGroup()
                                    .addGap(18, 18, 18)
                                    .addComponent(buttonBackup))))
                        .addGroup(panelLayout.createSequentialGroup()
                            .addGap(85, 85, 85)
                            .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(buttonAltPass, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(buttonAltUsername, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(buttonAltEmail, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(panelLayout.createSequentialGroup()
                            .addGap(34, 34, 34)
                            .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(checkExbProd)
                                .addComponent(checkStartLogin)
                                .addGroup(panelLayout.createSequentialGroup()
                                    .addGap(73, 73, 73)
                                    .addComponent(labelAdicionais))))
                        .addGroup(panelLayout.createSequentialGroup()
                            .addGap(69, 69, 69)
                            .addComponent(labelInfConta))
                        .addGroup(panelLayout.createSequentialGroup()
                            .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(panelLayout.createSequentialGroup()
                                    .addComponent(labelQntProd)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtQntProd))
                                .addGroup(panelLayout.createSequentialGroup()
                                    .addComponent(labelQntVali)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtQntVali, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                )
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(buttonMoreProd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(buttonMoreVali))))
                        .addGroup(panelLayout.createSequentialGroup()
                            .addGap(75, 75, 75)
                            .addComponent(buttonTxtProduto)))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonDesenvolver, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addComponent(separador, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelInfConta, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(buttonAltUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(buttonAltPass, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(buttonAltEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(separador1, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelBacukp)
                .addGap(18, 18, 18)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelQntProd)
                    .addComponent(txtQntProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonMoreProd))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelQntVali)
                    .addComponent(txtQntVali, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonMoreVali))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                
                .addGap(22, 22, 22)
                .addComponent(buttonBackup, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(buttonTxtVali, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(buttonTxtProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(separdor2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelAdicionais, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(checkExbProd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(checkStartLogin)
                .addGap(18, 18, 18)
                .addComponent(buttonDesenvolver, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGap(10, 10, 10)
                )
        );

        scrollPane.setViewportView(panel);
        scrollPane.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(labelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGap(10, 10, 10)
                .addComponent(labelTitulo)
                .addGap(17, 17, 17)
                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE))
        );

        scrollPane.getAccessibleContext().setAccessibleParent(buttonAltEmail);
    }// </editor-fold>                        


    // Variables declaration - do not modify                     
    private javax.swing.JButton buttonAltEmail;
    private javax.swing.JButton buttonAltPass;
    private javax.swing.JButton buttonAltUsername;
    private javax.swing.JButton buttonBackup;
    private javax.swing.JButton buttonDesenvolver;
    private javax.swing.JButton buttonMoreProd;
    private javax.swing.JButton buttonMoreVali;
    private javax.swing.JButton buttonTxtProduto;
    private javax.swing.JButton buttonTxtVali;
    private javax.swing.JCheckBox checkExbProd;
    private javax.swing.JCheckBox checkStartLogin;
    private javax.swing.JPanel panel;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JSeparator separador;
    private javax.swing.JLabel labelAdicionais;
    private javax.swing.JLabel labelBacukp;
    private javax.swing.JLabel labelInfConta;
    private javax.swing.JLabel labelQntProd;
    private javax.swing.JLabel labelQntVali;
    private javax.swing.JLabel labelTitulo;
    private javax.swing.JSeparator separador1;
    private javax.swing.JSeparator separdor2;
    private javax.swing.JTextField txtQntProd;
    private javax.swing.JTextField txtQntVali;

	public void manipularComponents()
    {
        Component components[] = panel.getComponents();

        for (Component c: components)
        {
            if (c instanceof JButton)
            {
                ((JButton) c).setCursor(new Cursor(Cursor.HAND_CURSOR));
                ((JButton) c).setFocusCycleRoot(true);
                ((JButton) c).setFocusPainted(false);;
            	
            }
        } 
    }
	
	public javax.swing.JButton getButtonAltEmail() {
		return buttonAltEmail;
	}

	public void setButtonAltEmail(javax.swing.JButton buttonAltEmail) {
		this.buttonAltEmail = buttonAltEmail;
	}

	public javax.swing.JButton getButtonAltPass() {
		return buttonAltPass;
	}

	public void setButtonAltPass(javax.swing.JButton buttonAltPass) {
		this.buttonAltPass = buttonAltPass;
	}

	public javax.swing.JButton getButtonAltUsername() {
		return buttonAltUsername;
	}

	public void setButtonAltUsername(javax.swing.JButton buttonAltUsername) {
		this.buttonAltUsername = buttonAltUsername;
	}

	public javax.swing.JButton getButtonBackup() {
		return buttonBackup;
	}

	public void setButtonBackup(javax.swing.JButton buttonBackup) {
		this.buttonBackup = buttonBackup;
	}

	public javax.swing.JButton getButtonDesenvolver() {
		return buttonDesenvolver;
	}

	public void setButtonDesenvolver(javax.swing.JButton buttonDesenvolver) {
		this.buttonDesenvolver = buttonDesenvolver;
	}

	public javax.swing.JButton getButtonMoreProd() {
		return buttonMoreProd;
	}

	public void setButtonMoreProd(javax.swing.JButton buttonMoreProd) {
		this.buttonMoreProd = buttonMoreProd;
	}

	public javax.swing.JButton getButtonMoreVali() {
		return buttonMoreVali;
	}

	public void setButtonMoreVali(javax.swing.JButton buttonMoreVali) {
		this.buttonMoreVali = buttonMoreVali;
	}

	public javax.swing.JButton getButtonTxtProduto() {
		return buttonTxtProduto;
	}

	public void setButtonTxtProduto(javax.swing.JButton buttonTxtProduto) {
		this.buttonTxtProduto = buttonTxtProduto;
	}

	public javax.swing.JButton getButtonTxtVali() {
		return buttonTxtVali;
	}

	public void setButtonTxtVali(javax.swing.JButton buttonTxtVali) {
		this.buttonTxtVali = buttonTxtVali;
	}

	public javax.swing.JCheckBox getCheckExbProd() {
		return checkExbProd;
	}

	public void setCheckExbProd(javax.swing.JCheckBox checkExbProd) {
		this.checkExbProd = checkExbProd;
	}

	public javax.swing.JCheckBox getCheckStartLogin() {
		return checkStartLogin;
	}

	public void setCheckStartLogin(javax.swing.JCheckBox checkStartLogin) {
		this.checkStartLogin = checkStartLogin;
	}


	public javax.swing.JPanel getPanel() {
		return panel;
	}

	public void setPanel(javax.swing.JPanel panel) {
		this.panel = panel;
	}

	public javax.swing.JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(javax.swing.JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public javax.swing.JSeparator getSeparador() {
		return separador;
	}

	public void setSeparador(javax.swing.JSeparator separador) {
		this.separador = separador;
	}

	public javax.swing.JLabel getLabelAdicionais() {
		return labelAdicionais;
	}

	public void setLabelAdicionais(javax.swing.JLabel labelAdicionais) {
		this.labelAdicionais = labelAdicionais;
	}

	public javax.swing.JLabel getLabelBacukp() {
		return labelBacukp;
	}

	public void setLabelBacukp(javax.swing.JLabel labelBacukp) {
		this.labelBacukp = labelBacukp;
	}

	public javax.swing.JLabel getLabelInfConta() {
		return labelInfConta;
	}

	public void setLabelInfConta(javax.swing.JLabel labelInfConta) {
		this.labelInfConta = labelInfConta;
	}


	public javax.swing.JLabel getLabelQntProd() {
		return labelQntProd;
	}

	public void setLabelQntProd(javax.swing.JLabel labelQntProd) {
		this.labelQntProd = labelQntProd;
	}

	public javax.swing.JLabel getLabelQntVali() {
		return labelQntVali;
	}

	public void setLabelQntVali(javax.swing.JLabel labelQntVali) {
		this.labelQntVali = labelQntVali;
	}

	public javax.swing.JLabel getLabelTitulo() {
		return labelTitulo;
	}

	public void setLabelTitulo(javax.swing.JLabel labelTitulo) {
		this.labelTitulo = labelTitulo;
	}

	public javax.swing.JSeparator getSeparador1() {
		return separador1;
	}

	public void setSeparador1(javax.swing.JSeparator separador1) {
		this.separador1 = separador1;
	}

	public javax.swing.JSeparator getSepardor2() {
		return separdor2;
	}

	public void setSepardor2(javax.swing.JSeparator separdor2) {
		this.separdor2 = separdor2;
	}

	public javax.swing.JTextField getTxtQntProd() {
		return txtQntProd;
	}

	public void setTxtQntProd(javax.swing.JTextField txtQntProd) {
		this.txtQntProd = txtQntProd;
	}

	public javax.swing.JTextField getTxtQntVali() {
		return txtQntVali;
	}

	public void setTxtQntVali(javax.swing.JTextField txtQntVali) {
		this.txtQntVali = txtQntVali;
	}
}
