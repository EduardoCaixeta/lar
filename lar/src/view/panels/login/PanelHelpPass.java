package view.panels.login;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.BorderFactory;

@SuppressWarnings("serial")
public class PanelHelpPass extends javax.swing.JPanel {
	public PanelHelpPass() {
		initComponents();
	}
                     
	private void initComponents() {

		labelTitulo = new javax.swing.JLabel();
		txtEmail = new javax.swing.JTextField();
		buttonSend = new javax.swing.JButton();
		buttonVoltar = new javax.swing.JButton();
		txtStatus = new javax.swing.JTextField();

		txtStatus.setEditable(false);
		txtStatus.setBorder(null);
		txtStatus.setOpaque(false);
		
		labelTitulo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelTitulo.setText("Insira o endere�o de email da sua conta:");

		txtEmail.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtEmail.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
    	txtEmail.setCursor(new Cursor(Cursor.TEXT_CURSOR));
    	txtEmail.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					buttonVoltar.doClick();
				}
			}
		});
		
		buttonSend.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonSend.setText("Prosseguir");
		buttonSend.setBorder(null);
        buttonSend.setContentAreaFilled(false);
        buttonSend.setCursor(new Cursor(Cursor.HAND_CURSOR));
        buttonSend.setDisabledIcon(null);
        buttonSend.setFocusCycleRoot(true);
        buttonSend.setFocusPainted(false);
        
		buttonVoltar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonVoltar.setText("Voltar");
		buttonVoltar.setBorder(null);
        buttonVoltar.setContentAreaFilled(false);
        buttonVoltar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        buttonVoltar.setDisabledIcon(null);
        buttonVoltar.setFocusCycleRoot(true);
        buttonVoltar.setFocusPainted(false);
		
		txtStatus.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(buttonVoltar)
	                        .addGap(0, 0, Short.MAX_VALUE))
	                    .addGroup(layout.createSequentialGroup()
	                        .addContainerGap()
	                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                            .addComponent(txtStatus, javax.swing.GroupLayout.Alignment.TRAILING)
	                            .addComponent(txtEmail))))
	                .addContainerGap())
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap(40, Short.MAX_VALUE)
	                .addComponent(labelTitulo)
	                .addContainerGap(41, Short.MAX_VALUE))
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addComponent(buttonSend, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addGap(53, 53, 53)
	                .addComponent(labelTitulo)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(18, 18, 18)
	                .addComponent(buttonSend)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
	                .addComponent(buttonVoltar))
	        );
	}// </editor-fold>


// Variables declaration - do not modify                     
	private javax.swing.JButton buttonSend;
	private javax.swing.JButton buttonVoltar;
	private javax.swing.JLabel labelTitulo;
	private javax.swing.JTextField txtEmail;
	private javax.swing.JTextField txtStatus;
// End of variables declaration                   
	public javax.swing.JButton getButtonSend() {
		return buttonSend;
	}

	public void setButtonSend(javax.swing.JButton buttonSend) {
		this.buttonSend = buttonSend;
	}

	public javax.swing.JButton getButtonVoltar() {
		return buttonVoltar;
	}

	public void setButtonVoltar(javax.swing.JButton buttonVoltar) {
		this.buttonVoltar = buttonVoltar;
	}

	public javax.swing.JLabel getLabelTitulo() {
		return labelTitulo;
	}

	public void setLabelTitulo(javax.swing.JLabel labelTitulo) {
		this.labelTitulo = labelTitulo;
	}

	public javax.swing.JTextField getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(javax.swing.JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}

	public javax.swing.JTextField getTxtStatus() {
		return txtStatus;
	}

	public void setTxtStatus(javax.swing.JTextField txtStatus) {
		this.txtStatus = txtStatus;
	}
}