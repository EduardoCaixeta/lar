package view;

import javax.swing.WindowConstants;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import view.forms.*;
import view.forms.configs.FormConfig;
import view.forms.login.FormLogin;
import view.panels.PanelCadastro;
import view.panels.PanelConfig;
import view.panels.login.PanelLogin;
import model.Database;
import model.entity.Users;

@SuppressWarnings("serial")
public class Frame extends JFrame {

	

	
	
	private MenuBarra menuBarra;
	private JPanel form;
	private String codUser;
	private String nameDBU = "users.db";
	private Users user;
	
	private String nameDBP;
	private String nameDBV;
	private String nameDBL;
	
	public Frame() {
		menuBarra = new MenuBarra();
		menuBarra.getButtonCadastrar().addActionListener(e -> setCadastro());
		setSize(650, 530);
		menuBarra.getButtonConsulta().addActionListener(e -> setConsulta());
		menuBarra.getButtonConfig().addActionListener(e -> setConfig());
		setLogin();
		centerForm();
	}
	
	public void setForm(JPanel form) {
		int width = getWidth();
		int height = getHeight();
		int x = getX();
		int y = getY();
		getContentPane().removeAll();
		this.form = form;
		initComponents();
		this.validate();
		setSize(width, height);
		setLocation(x, y);
	}
	
	
	public void setHome(Users user)
	{
		this.user = user;
		this.codUser = String.valueOf(user.getId());
		nameDBP = "Prod"+codUser+".db";
		nameDBV = "Val"+codUser+".db";
		nameDBL = "locales"+codUser+".db";
		setCadastro();
	}
	
	public void setLogin() {
		try
		{
			FormLogin formLogin = new FormLogin(new PanelLogin(),this);
			this.add(formLogin.getPanel());
			this.pack();
			this.setSize(320, 250);
			getAccessibleContext().setAccessibleParent(this);
			setResizable(false);
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this,e.getMessage(),"Algo deu errado", 
	                JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
		}
	}

	public void setCadastro() {
		
		setForm(new FormCadastro(new Database(nameDBP), new Database(nameDBV), this).getPanel());
	}

public void setAddCadastro() {
		
		FormCadastro addForm = new FormCadastro(new Database(nameDBP), new Database(nameDBV), this);
		addForm.add_Click();
		setForm(addForm.getPanel());
	}
	
	public void setConsulta() {
		
		setForm(new FormConsulta(new Database(nameDBP), new Database(nameDBV), this).getPanel());
		
	}

	public void setConfig() {
		setForm(new FormConfig(new PanelConfig(this), this).getPanel());
	}

	public void paintComponents(Graphics g) {
		g.setColor(new Color(255, 255, 255));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
	}

	public void centerForm() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		int x = (dim.width - getWidth()) / 2;
		int y = (dim.height - getHeight()) / 2;

		setLocation(x, y);
	}
	
	public static void main(String args[]) {
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>

		new Frame().setVisible(true);

	}
	
	private void initComponents() {
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addComponent(menuBarra, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addComponent(form, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(menuBarra, javax.swing.GroupLayout.PREFERRED_SIZE, 23,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGap(0, 0, 0)
						.addComponent(form, javax.swing.GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)));

		getAccessibleContext().setAccessibleParent(this);

		pack();

		centerForm();
	}// </editor-fold>

	public MenuBarra getMenuBarra() {
		return this.menuBarra;
	}

	public String getCodUser() {
		return codUser;
	}

	public void setCodUser(String codUser) {
		this.codUser = codUser;
	}

	public JPanel getForm() {
		return form;
	}

	public void setMenuBarra(MenuBarra menuBarra) {
		this.menuBarra = menuBarra;
	}

	public String getNameDBU() {
		return nameDBU;
	}

	public void setNameDBU(String nameDBU) {
		this.nameDBU = nameDBU;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public String getNameDBP() {
		return nameDBP;
	}

	public String getNameDBV() {
		return nameDBV;
	}

	public String getNameDBL() {
		return nameDBL;
	}

	
}
