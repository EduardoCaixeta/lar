package view;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
@SuppressWarnings("serial")
public class MenuBarra extends JPanel {

    /**
     * Creates new form MenuBar
     */
    public MenuBarra() {
        initComponents();
    }

    public void paintComponent(Graphics g)
    {
        g.setColor(new Color(255, 255, 255));
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
    }

    public void desableMenu()
    {
        buttonHome.setContentAreaFilled(false);
        buttonConsulta.setContentAreaFilled(false);
        buttonCadastrar.setContentAreaFilled(false);
        buttonConfig.setContentAreaFilled(false);
        buttonHelp.setContentAreaFilled(false);
    }

    public void clickButton(String butt)
    {
    	buttonHome.setCursor(new Cursor(Cursor.HAND_CURSOR));
    	buttonCadastrar.setCursor(new Cursor(Cursor.HAND_CURSOR));
    	buttonConsulta.setCursor(new Cursor(Cursor.HAND_CURSOR));
    	buttonConfig.setCursor(new Cursor(Cursor.HAND_CURSOR));
    	buttonHelp.setCursor(new Cursor(Cursor.HAND_CURSOR));
    	buttonProdutos.setCursor(new Cursor(Cursor.HAND_CURSOR));
    	if(butt.equals("HOME")) buttonHome.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    	else if(butt.equals("CADASTRO")) buttonCadastrar.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    	else if(butt.equals("CONSULTA")) buttonConsulta.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    	else if(butt.equals("CONFIG")) buttonConfig.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    	else if(butt.equals("HELP")) buttonHelp.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    	else if(butt.equals("PRODUTOS")) buttonProdutos.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    	
    }
    
    private void initComponents() {

        buttonHome = new JButton();
        buttonCadastrar = new JButton();
        buttonConsulta = new JButton();
        buttonConfig = new JButton();
        buttonHelp = new JButton();
        buttonProdutos = new JButton();

        buttonHome.setText("HOME");
        buttonHome.setBorderPainted(false);
        buttonHome.setOpaque(true);
        buttonHome.setFocusPainted(false);
        buttonHome.setFont(new Font("Arial", 0, 11));

        buttonCadastrar.setText("CADASTRAR");
        buttonCadastrar.setBorderPainted(false);
        buttonCadastrar.setOpaque(false);
        buttonCadastrar.setFocusPainted(false);
        buttonCadastrar.setFont(new Font("Arial", 0, 11));

        buttonConsulta.setText("CONSULTA");
        buttonConsulta.setBorderPainted(false);
        buttonConsulta.setOpaque(false);
        buttonConsulta.setFocusPainted(false);
        buttonConsulta.setFont(new Font("Arial", 0, 11));
        
        buttonProdutos.setText("PRODUTOS");
        buttonProdutos.setBorderPainted(false);
        buttonProdutos.setOpaque(false);
        buttonProdutos.setFocusPainted(false);
        buttonProdutos.setFont(new Font("Arial",0,11));

        buttonConfig.setText("CONFIGURAÇÕES");
        buttonConfig.setBorderPainted(false);
        buttonConfig.setOpaque(false);
        buttonConfig.setFocusPainted(false);
        buttonConfig.setFont(new Font("Arial", 0, 11));

        buttonHelp.setText("HELP");
        buttonHelp.setBorderPainted(false);
        buttonHelp.setOpaque(false);
        buttonHelp.setFocusPainted(false);
        buttonHelp.setFont(new Font("Arial", 0, 11));

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(buttonHome, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(22, 22, 22)
                    .addComponent(buttonCadastrar)
                    .addGap(22, 22, 22)
                    .addComponent(buttonConsulta)
                    .addGap(22, 22, 22)
                    .addComponent(buttonProdutos, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(22, 22, 22)
                    .addComponent(buttonConfig)
                    .addGap(22, 22, 22)
                    .addComponent(buttonHelp)
                    .addContainerGap(28, Short.MAX_VALUE))
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(buttonHome)
                        .addComponent(buttonCadastrar)
                        .addComponent(buttonConsulta)
                        .addComponent(buttonConfig)
                        .addComponent(buttonHelp)
                        .addComponent(buttonProdutos)))
            );
        desableMenu();
    }// </editor-fold>                        

    // Variables declaration - do not modify                     
    private JButton buttonCadastrar;
    private JButton buttonConfig;
    private JButton buttonConsulta;
    private JButton buttonHelp;
    private JButton buttonHome;
    private JButton buttonProdutos;
    // End of variables declaration                   

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie buttonCadastrar*/
    public JButton getButtonCadastrar(){
        return this.buttonCadastrar;
    }//end method getButtonCadastrar

    /**SET Method Propertie buttonCadastrar*/
    public void setButtonCadastrar(JButton buttonCadastrar){
        this.buttonCadastrar = buttonCadastrar;
    }//end method setButtonCadastrar

    /**GET Method Propertie buttonConfig*/
    public JButton getButtonConfig(){
        return this.buttonConfig;
    }//end method getButtonConfig

    /**SET Method Propertie buttonConfig*/
    public void setButtonConfig(JButton buttonConfig){
        this.buttonConfig = buttonConfig;
    }//end method setButtonConfig

    /**GET Method Propertie buttonConsulta*/
    public JButton getButtonConsulta(){
        return this.buttonConsulta;
    }//end method getButtonConsulta

    /**SET Method Propertie buttonConsulta*/
    public void setButtonConsulta(JButton buttonConsulta){
        this.buttonConsulta = buttonConsulta;
    }//end method setButtonConsulta

    /**GET Method Propertie buttonHelp*/
    public JButton getButtonHelp(){
        return this.buttonHelp;
    }//end method getButtonHelp

    /**SET Method Propertie buttonHelp*/
    public void setButtonHelp(JButton buttonHelp){
        this.buttonHelp = buttonHelp;
    }//end method setButtonHelp

    /**GET Method Propertie buttonHome*/
    public JButton getButtonHome(){
        return this.buttonHome;
    }//end method getButtonHome

    /**SET Method Propertie buttonHome*/
    public void setButtonHome(JButton buttonHome){
        this.buttonHome = buttonHome;
    }//end method setButtonHome

    //End GetterSetterExtension Source Code
    //!
}