package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.ArrayList;
import java.util.List;
import model.entity.Users;

public class UsersDao {
	public static Database database;
	public static Dao<Users, Integer> dao;
	private List<Users> listaUserss;

	public UsersDao(Database database) throws Exception {
		UsersDao.setDatabase(database);
		listaUserss = new ArrayList<Users>();
	}

	public static void setDatabase(Database database) throws Exception {
		UsersDao.database = database;
		try {
			dao = DaoManager.createDao(database.getConnection(), Users.class);
			TableUtils.createTableIfNotExists(database.getConnection(), Users.class);
		} catch (SQLException e) {
			throw new Exception("N�o foi poss�vel abrir a Database usu�rios.");
		}
	}

	public void create(Users user) throws Exception {
		try {
			String emailUP = "";
			for (int i = 0; i < user.getEmail().length(); i++) {
				try {
					emailUP += Character.toUpperCase(user.getEmail().charAt(i));
				} catch (Exception e) {
					emailUP += user.getEmail().charAt(i);
				}
			}
			user.setEmail(emailUP);
			int nrows = 0;
			nrows = dao.create(user);
			if (nrows == 0)
				throw new Exception();
		} catch (Exception e) {
			throw new Exception("Erro ao criar o cadastro.");
		}
	}

	public boolean existUsers(Users user) throws Exception {
		try {
			String name = user.getUsername();
			listaUserss = dao.queryForAll();
			for (int i = 0; i < listaUserss.size(); i++) {
				if (name.equals(listaUserss.get(i).getUsername())
						&& user.getPassword().equals(listaUserss.get(i).getPassword())) {
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			throw new Exception("N�o foi poss�vel verificar o usu�rio.");
		}
	}

	public Users getUser(String dado, String tipo) throws Exception
	{
		if(tipo.equals("username"))
		{
			try {
				String name = dado;
				int id = -1;
				listaUserss = dao.queryForAll();
				for (int i = 0; i < listaUserss.size(); i++) {
					if (name.equals(listaUserss.get(i).getUsername())) {
						id = i;
						break;
					}
				}
				if (id != -1)
				{
					Users u = dao.queryForId(id+1);
					return u;
				}
				else 
					throw new Exception("~");
			} catch (Exception e) {
				if(e.getMessage().equals("~")) throw new Exception("N�o h� cadastros com este nome de usu�rio.");
				else throw new Exception("N�o foi poss�vel verificar o nome de usu�rio.");
			}
		}
		else if( tipo.equals("email"))
		{
			try {
				String emailUP = "";
				for (int i = 0; i < dado.length(); i++) {
					try {
						emailUP += Character.toUpperCase(dado.charAt(i));
					} catch (Exception e) {
						emailUP += dado.charAt(i);
					}
				}
				int id = -1;
				listaUserss = dao.queryForAll();
				for (int i = 0; i < listaUserss.size(); i++) {
					if (emailUP.equals(listaUserss.get(i).getEmail())) {
						id = i;
						break;
					}
				}
				if (id != -1)
				{
					Users u = dao.queryForId(id+1);
					return u;
				}
				else 
					throw new Exception("~");
			
			} catch (Exception e) {
				if(e.getMessage().equals("~")) throw new Exception("N�o h� cadastros com este endere�o de email.");
				else throw new Exception("N�o foi poss�vel verificar o email.");
			}
		}
		else throw new Exception("Tipo de dado inv�lido.");
	}
	
	public boolean existUsername(String name) throws Exception {
		try {
		
			listaUserss = dao.queryForAll();
			for (int i = 0; i < listaUserss.size(); i++) {
				if (name.equals(listaUserss.get(i).getUsername())) {
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			throw new Exception("N�o foi poss�vel verificar o usu�rio.");
		}
	}

	public void update(Users user) throws Exception {
		try {
			
			String emailUP = "";
			for (int i = 0; i < user.getEmail().length(); i++) {
				try {
					emailUP += Character.toUpperCase(user.getEmail().charAt(i));
				} catch (Exception e) {
					emailUP += user.getEmail().charAt(i);
				}
			}
			user.setEmail(emailUP);
			dao.update(user);
		} catch (Exception e) {
			throw new Exception("N�o foi poss�vel atualizar o usu�rio.");
		}
	}

	public boolean existEmail(String email) throws Exception {
		try {
			String emailUP = "";
			for (int i = 0; i < email.length(); i++) {
				try {
					emailUP += Character.toUpperCase(email.charAt(i));
				} catch (Exception e) {
					emailUP += email.charAt(i);
				}
			}
			listaUserss = dao.queryForAll();
			for (int i = 0; i < listaUserss.size(); i++) {
				if (emailUP.equals(listaUserss.get(i).getEmail())) {
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			throw new Exception("N�o foi poss�vel verificar o email.");
		}

	}


	public int registros() throws Exception {
		try {
			listaUserss = dao.queryForAll();
			return listaUserss.size();
		} catch (Exception e) {
			throw new Exception("N�o foi poss�vel ler os registros.");
		}
	}
}
